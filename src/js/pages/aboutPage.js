$('.section-experience__slider').slick({
  autoplay: true,
  autoplaySpeed: 4000,
  dots: true,
  arrows: false,
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
})

$('.section-certificates__slider').slick({
  autoplay: true,
  autoplaySpeed: 4000,
  dots: true,
  arrows: false,
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1
})

$('.partners-slider').slick({
  autoplay: true,
  autoplaySpeed: 4000,
  dots: true,
  arrows: false,
  infinite: true,
  slidesToShow: 4,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 2
      }
    },
  ]
})

$('.section-certificates__slider').on('beforeChange', function (e, slick, currentSlide, nextSlide) {
  if (nextSlide < currentSlide) {
    $('.section-certificates__slider .slick-dots li').addClass('to-right')
  } else {
    $('.section-certificates__slider .slick-dots li').removeClass('to-right')
  }
})
