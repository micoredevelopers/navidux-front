const setMainNewImageHeight = () => {
  $(document).ready(function () {
    const sectionH = $('.news-section').height() + 285
    const newsImg = $('.news-image')

    newsImg.height(sectionH)
  })
}

$('.solution-slider').slick({
  autoplay: true,
  autoplaySpeed: 4000,
  dots: true,
  arrows: false,
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
})

$('.solution-desktop-slider').slick({
  autoplay: true,
  autoplaySpeed: 4000,
  dots: true,
  arrows: false,
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1
})

$('.solution-slider').on('beforeChange', function (e, slick, currentSlide, nextSlide) {
  if (nextSlide < currentSlide) {
    $('.solution-slider .slick-dots li').addClass('to-right')
  } else {
    $('.solution-slider .slick-dots li').removeClass('to-right')
  }
})

$('.project-section__slider').slick({
  autoplay: true,
  autoplaySpeed: 4000,
  dots: true,
  fade: true,
  arrows: false,
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 992,
      settings: {
        fade: false
      }
    }
  ]
})

$('.about-section__slider').slick({
  autoplay: true,
  autoplaySpeed: 4000,
  dots: true,
  arrows: false,
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
})

$('.solution-section .nav-link').hover(function () {
  const dataID = $(this).attr('data-target')

  $('.solution-section .nav-link').removeClass('active')
  $(this).addClass('active')

  $('.solution-section .tab-panel').removeClass('show')
  $(`.solution-section ${dataID}`).addClass('show')
})

let count = 0

$(window).on('scroll', function () {
  let hT = $('.project-image').offset().top,
    hH = $('.project-image').outerHeight(),
    wS = $(this).scrollTop() +  + $(this).height()
    count = 0

  if (wS > hT && wS < (hT + hH)){
    count++
    console.log(count)
    console.log('el on view!');
  }
})

setMainNewImageHeight()
