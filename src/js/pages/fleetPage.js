const img = $('.big-fleet-image')
const rowW = $('.row-fleet').width() - 30
const offsetWidth = window.innerWidth * 0.125

img.css({
  width: `calc(100% - ${offsetWidth + (rowW * 0.25) - 15}px)`
})

$(window).resize(function () {
  const rowW = $('.row-fleet').width() - 30
  const offsetWidth = window.innerWidth * 0.125

  img.css({
    width: `calc(100% - ${offsetWidth + (rowW * 0.25) - 15}px)`
  })
})

$('.fleet-category__name').on('click', function () {
  const id = $(this).attr('data-tab-target')

  if ($(this).hasClass('active')) {
    $('.fleet-category__name').removeClass('active')
    img.removeClass('hide')
    $('.tab-pane').removeClass('show')
  } else {
    $('.fleet-category__name').removeClass('active')
    $('.tab-pane').removeClass('show')

    $(this).toggleClass('active')
    img.addClass('hide')

    setTimeout(() => {
      $(`${id}`).toggleClass('show')
    }, 800)
  }
})

$('.fleet-category-slider').slick({
  autoplay: true,
  autoplaySpeed: 4000,
  dots: true,
  arrows: false,
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1
})
