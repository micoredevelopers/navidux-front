const services = [
  {
    infoTitle: 'Commercial Management',
    infoDesc: 'Navidux can provide most efficient management of clients’ fleet due to long presence and experience of our personnel in ' +
      'chartering and partnership with first class charterers.',
    subServices: [
      {
        title: 'Actual voyage results calculations'
      },
      {
        title: 'Evaluation best course and speed'
      },
      {
        title: 'Performance analyses'
      },
      {
        title: 'Maximize clients profit & minimize risks'
      }
    ]
  },
  {
    infoTitle: 'Operating',
    infoDesc: 'Fleet operation is always performed by our qualified personnel with many years experience and close cooperation in agency, ' +
      'ship chandlering, survey, etc.',
    subServices: [
      {
        title: 'Optimization'
      },
      {
        title: 'Control and monitoring',
        desc: 'Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.' +
          'Nihil anim keffiyeh',
      },
      {
        title: 'The Effective solution to difficult tasks'
      }
    ]
  },
  {
    infoTitle: 'Chartering',
    infoDesc: 'Chartering of our clients’ vessel is managed by our in-house ship broker team. We collaborate with large broking companies ' +
      'and forwarders worldwide. Fixing terms for chartered tonnage are prepared in way to be effective for our clients.',
    subServices: [
      {
        title: 'Safety priority'
      },
      {
        title: 'Fast shipping'
      },
      {
        title: 'Competitive rates'
      },
      {
        title: 'Reliable agents'
      }
    ]
  },
  {
    infoTitle: 'Survey',
    infoDesc: 'Our surveyor team provides all services in part of cargo and vessel inspection.',
    subServices: [
      {
        title: 'Special Survey'
      },
      {
        title: 'Annual Survey'
      },
      {
        title: 'Additional Survey'
      },
      {
        title: 'Periodical Survey'
      }
    ]
  }
]

const serviceCol = $('.col-service-item')

serviceCol.each((indexService, item) => {
  const row = $(item).find('.row')

  $(row).append(`
    <div class="n-col-8 n-col-lg-4 n-offset-lg-1 col-service-info">
      <div class="service-info-wrap">
        <h5 class="service-name d-none d-lg-block">${services[indexService].infoTitle}</h5>
        <p class="service-desc">${services[indexService].infoDesc}</p>
        <div class="row row-services"></div>
      </div>
    </div>
  `)

  services[indexService].subServices.map((subService) => {
    $(row).find('.row-services').append(`
        <div class="n-col-8 n-col-lg-4">
          <h3 class="service-collapse-link">
            <img src="images/Oval.svg" alt="Service collapse">
            <span>${subService.title}</span>
          </h3>
        </div>
      `)
  })
})
