const slider = $('.col-project-slider')
const rowW = $('.row-project').width() - 30
const offsetWidth = window.innerWidth * 0.125

$('.project-slider').slick({
  autoplay: true,
  autoplaySpeed: 4000,
  dots: true,
  arrows: false,
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1
})

slider.css({ width: `calc(100% - ${offsetWidth + (rowW * 0.25)}px)` })

$(window).resize(function () {
  const rowW = $('.row-project').width() - 30
  const offsetWidth = window.innerWidth * 0.125

  slider.css({ width: `calc(100% - ${offsetWidth + (rowW * 0.25)}px)` })
})

var $window = $(window);
var $box = $(".col-project-slider");

// $window.scroll(function() {
//   var scrollTop = $window.scrollTop();
//
//   if (scrollTop >= 250) {
//     $box.css({top: -250 + scrollTop});
//   }
// });
