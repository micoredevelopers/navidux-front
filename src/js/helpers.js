$('.header__menu').click(function () {
  $('.global-menu').toggleClass('active')
  $('html, body').addClass('overflow-hidden')
})

$('.global-menu__close').click(function () {
  $('.global-menu').toggleClass('active')
  $('html, body').removeClass('overflow-hidden')
})

$('.lang-select').click(function () {
  $(this).toggleClass('is-selected')
})

$('form').on('submit', function (e) {
  e.preventDefault()
  const data = {}
  const url = $(this).attr('action')
  const inputs = $(this).find('input')

  inputs.each((index, input) => {
    const $input = $(input)
    const name  = $input.attr('name')
    const value  = $input.val()

    data[name] = value
  })

  $.ajax(url, {
    method: 'POST',
    data
  }).done(function (res) {
    /* SUCCESS EVENT */
  })
})

$('.feedback-btn').on('click', function () {
  $('.feedback-modal').addClass('active')
})

$('.modal-close').on('click', function () {
  $('.feedback-modal').removeClass('active')
  $('.feedback-modal').find('.form-wrap input').val('')
})

const convertImages = (query, callback) => {
  const images = document.querySelectorAll(query);

  images.forEach(image => {
    fetch(image.src)
      .then(res => res.text())
      .then(data => {
        const parser = new DOMParser();
        const svg = parser.parseFromString(data, 'image/svg+xml').querySelector('svg');

        if (image.id) svg.id = image.id;
        if (image.className) svg.classList = image.classList;

        image.parentNode.replaceChild(svg, image);
      })
      .then(callback)
      .catch(error => console.error(error))
  });
}

$(document).ready(function () {
  convertImages('.svg')

  $('a[href]').on('click', function (e) {
    let href = $(this).attr('href')
    e.preventDefault()
    if (href.includes('.html') || href === '/') {
      $('.main-overlay').addClass('active')
      setTimeout(() => {
        window.location = $(this).attr('href')
      }, 1100)
    }
  })
})
