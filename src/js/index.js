require('./helpers')

const main = document.querySelector('main')
const pageId = main.getAttribute('id')
let scrollPos = 0

AOS.init({
  once: true,
  duration: 1000
})

if (($('body').scrollTop() || $('html').scrollTop()) > $('body').height()) {
  $('header').addClass('active')
} else {
  $('header').removeClass('active')
}

if (pageId !== 'main-page') {
  $('header').addClass('active')
} else {
  $(window).on('scroll', function () {
    if ($(window).scrollTop() > $(window).height()) {
      $('header').addClass('active')
    } else {
      $('header').removeClass('active')
    }
  })
}

window.onscroll = function () {
  let st = $(this).scrollTop()

  if (st > scrollPos) {
    $('header').addClass('hide')
  } else {
    $('header').removeClass('hide')
  }

  scrollPos = st
}

switch (pageId) {
  case 'main-page':
    require('./pages/mainPage.js')
    break
  case 'about-page':
    require('./pages/aboutPage.js')
    break
  case 'services-page':
    require('./pages/servicesPage')
    break
  case 'projects-page':
    require('./pages/projectsPage')
    break
  case 'fleet-page':
    require('./pages/fleetPage')
    break
}
