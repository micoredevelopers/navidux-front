const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = () => new CopyWebpackPlugin([
  { from: './src/fonts', to: '../dist/fonts' },
  { from: './src/favicon.svg', to: '../dist/favicon.svg' },
  { from: './src/images', to: '../dist/images' }
])
