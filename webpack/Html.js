const fs = require('fs')
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = () => {
  const templateFiles = fs.readdirSync(path.resolve(__dirname, '../src/html/pages'))

  return templateFiles.map(item => {
    const parts = item.split('.')
    const extension = parts[1]
    const name = parts[0]

    return new HtmlWebpackPlugin({
      inject: false,
      filename: `${name}.html`,
      template: path.resolve(__dirname, `../src/html/pages/${name}.${extension}`)
    })
  })
}
